package sk.greate43.bucketdrops.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by great on 8/30/2016.
 */

public class NoItemViewHolder extends RecyclerView.ViewHolder {
    public NoItemViewHolder(View itemView) {
        super(itemView);
    }
}
